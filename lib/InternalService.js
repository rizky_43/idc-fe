const services = require('./WebService')
const api = services.api
const log = serverUtils.getLogger('lib.InternalService')
const config = require('./Config')

//API Common

exports.predefinedQuery = function(data, res, next, callback, fallout) {
  log.info(`[POST] /execute/${data.collection}/${data.type}/${data.name} - data: ${JSON.stringify(data)}`)
  return api
    .post(`/execute/${data.collection}/${data.type}/${data.name}`, data.body, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (err.response)
        log.error(`[ERROR] /execute/${data.collection}/${data.type}/${data.name} - [%s:%s] %s\ndata = %o`, err.response.status, err.response.statusText, err.message, data)
      else 
        log.error(`[ERROR] /execute/${data.collection}/${data.type}/${data.name} - [%s]\ndata = %o`, err.message, data)
      if (fallout) {
        fallout(err)
      } else {
        if (err.response.status === 404) {
          callback({nodes: []})
        } else {
          next(err)}
      }
    })
}

exports.listNodes = function(data, res, next, callback, fallout) {
  log.info(`[POST] /internal/node/list - data: ${JSON.stringify(data)}`)
  return api
    .post('/internal/node/list', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (err.response)
        log.error('[ERROR] /internal/node/list - [%s:%s] %s\ndata = %o', err.response.status, err.response.statusText, err.message, data)
      else 
        log.error('[ERROR] /internal/node/list - [%s]\ndata = %o', err.message, data)
      if (fallout) {
        fallout(err)
      } else {
        if (err.response.status === 404) {
          callback({nodes: []})
        } else {
          next(err)}
      }
    })
}

exports.createNode = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/node')
  log.debug(data)
  return api
    .post('/internal/node', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.cypher = function(data, res, next, callback, fallout) {
  log.info('[POST] /internal/cypher')
  log.info(data)
  return api
    .post('/internal/cypher', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNode = function(nodeId, includeRelation, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=${includeRelation}`)
  return api
    .get(`/internal/node?id=${nodeId}&includeRelations=${includeRelation}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodesByLabelPath = function(nodeId, nodeLabel, filter, labels, res, next, callback, fallout) {
  log.debug(`[GET] /internal/nodesByLabelPath?id=${nodeId}&nodeLabel=${nodeLabel}&labels=${labels}&filter=${filter}`)
  return api
    .get(`/internal/nodesByLabelPath?id=${nodeId}&nodeLabel=${nodeLabel}&labels=${labels}&filter=${filter}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.getNodeSelectedRelationship = function(nodeId, relationshipTypes, res, next, callback, fallout) {
  log.debug(`[GET] /internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`)
  return api
    .get(`/internal/node?id=${nodeId}&includeRelations=true&relationshipTypes=${relationshipTypes}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.updateNode = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/node')
  log.debug(data)
  return api
    .put('/internal/node', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNode = function(nodeId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/node?id=${nodeId}`)
  return api
    .delete(`/internal/node?id=${nodeId}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeWithEdges = function(data, res, next, callback, fallout) {
  log.debug('[DELETE] /internal/nodeWithRelations')
  log.debug(data)
  return api
    .delete('/internal/nodeWithRelations', {data: data, headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}})
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.createRelationship = function(data, res, next, callback, fallout) {
  log.debug('[POST] /internal/relationship')
  log.debug(data)
  return api
    .post('/internal/relationship', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteRelationship = function(relationshipId, res, next, callback, fallout) {
  log.debug(`[DELETE] /internal/relationship?id=${relationshipId}`)
  return api
    .delete(`/internal/relationship?id=${relationshipId}`, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.deleteNodeProperties = function(nodeId, properties, res, next, callback, fallout) {
  let url = `/internal/node/property?id=${nodeId}`
  log.debug(`[DELETE] ${url}`)
  
  return api
    .delete(url, {
      data: properties,
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.moveRelationship = function(data, res, next, callback, fallout) {
  let url = `/internal/relationship/move?id=${data.id}`
  if (data.endNodeId) url += `&endNodeId=${data.endNodeId}`
  if (data.startNodeId) url += `&startNodeId=${data.startNodeId}`

  log.debug(`[PUT] ${url}`)

  return api
    .put(url, data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.updateRelationship = function(data, res, next, callback, fallout) {
  log.debug('[PUT] /internal/relationship')
  log.debug(data)
  return api
    .put('/internal/relationship', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

//API SPATIAL
exports.spatialList = function(data, res, next, callback, fallout) {
  log.debug('[POST] /spatial/list')
  log.debug(data)
  return api
    .post('/spatial/list', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
exports.spatialAdd = function(data, res, next, callback, fallout) {
  log.debug('[POST] /spatial/add')
  log.debug(data)
  return api
    .post('/spatial/add', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.spatialIntersect = function(data, res, next, callback, fallout) {
  log.debug('[POST] /spatial/intersect')
  log.debug(data)
  return api
    .post('/spatial/intersect', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.spatialDelete = function (data , res, next, callback, fallout){
  log.debug('[DELETE] /spatial/delete')
  log.debug(data)
  return api
    .delete('/spatial/delete', data, {
      headers: {'X-Requester': res.locals.userId, Authorization: 'Basic ' + Buffer.from(`${config.auth.clientId}:${config.auth.clientSecret}`).toString('base64')}
    })
    .then(function(resp) {
      callback(resp.data)
    })
    .catch(function(err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}