const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [{
  name: 'name',
  label: 'Name',
  link: 'view',
  filter: {
    type: 'text'
  },
  defaultSort: 'asc'
},
{
  name: 'type',
  label: 'Type',
  filter: {
    type: 'text'
  }
},
{
  name: 'uid',
  label: 'Site ID',
  filter: {
    type: 'text'
  }
},
{
  name: 'address',
  label: 'Address',
  filter: {
    type: 'text'
  }
},
{
  name: 'building',
  label: 'Building',
  filter: {
    type: 'text'
  }
},
{
  name: 'createdAt',
  label: 'Created At',
  type: 'date'
},
{
  name: 'createdBy',
  label: 'Created By',
  filter: {
    type: 'text'
  }
}
]

router.get('/data', function (req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Location',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          uid: item.node.uid ? item.node.uid : null,
          address: item.node.address ? item.node.address : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {
              url: '/location/' + item.node._id,
              method: 'GET'
            }
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function (req, res, next) {
  res.render('location/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {
        url: '/location/data',
        method: 'GET'
      }
    }
  })
})

router.post('/', function (req, res, next) {
  const reqData = {
    label: 'Location',
    properties: {
      name: req.body.name,
      type: req.body.type,
      uid: req.body.uid,
      address: req.body.address,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function (resData) {
    res.status(200).send({
      links: {
        view: {
          url: '/location/' + resData.node._id,
          method: 'GET'
        }
      }
    })
  })
})

router.put('/:_id', function (req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Location',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/location/' + _id,
          method: 'GET'
        }
      }
    })
  })
})


router.get('/:_id', function (req, res, next) {
  var id = req.params._id
  const optionsPredifineSpace = {
    collection: "IDC-FE",
    name: "GET_SPACE_INFO",
    type: "QUERY",
    body: {
      filter: {
        "1": parseInt(id)
      }
    }
  }
  internalService.predefinedQuery(optionsPredifineSpace, res, next, function (respData) {
    let location = respData[0].location
    let building = respData[0].building
    let floor = respData[0].floor
    let room = respData[0].room
    let space = respData[0].space
    let rack = respData[0].rack
    let result = {
      location: {},
      building: [],
      floor: [],
      room: [],
      rack: [],
      space: []
    }
    location['links'] = {
      delete: {
        url: '/location/' + id,
        method: 'DELETE'
      },
      edit: {
        url: '/location/' + id,
        method: 'PUT'
      }
    }
    result.location = location
    if (building.length > 0) {
      let temData = []
      building.forEach((i) => {
        const links = {
          view: {
            url: '/building/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/building/${i._id}/${id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.building = temData
    }
    if (floor.length > 0) {
      let temData = []
      floor.forEach((i) => {
        const links = {
          view: {
            url: '/floor/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/floor/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.floor = temData
    }
    if (room.length > 0) {
      let temData = []
      room.forEach((i) => {
        const links = {
          view: {
            url: '/room/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/room/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.room = temData
    }
    if (space.length > 0) {
      let temData = []
      space.forEach((i) => {
        const links = {
          view: {
            url: '/space/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/space/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.space = temData
    }
    if (rack.length > 0) {
      let temData = []
      rack.forEach((i) => {
        const links = {
          view: {
            url: '/rack/' + i._id + '?parentId=' + id,
            method: 'GET'
          },
          delete: {
            url: `/rack/${i._id}`,
            method: 'DELETE'
          }
        }
        i['links'] = links
        temData.push(i)
      })
      result.rack = temData
    }

    const optionsPredifine = {
      collection: "IDC-FE",
      type: "QUERY",
      name: "GET_ELECTRICITY_INFO",
      body: {
        filter: {
          "1": parseInt(id)
        }
      }
    }
    internalService.predefinedQuery(optionsPredifine, res, next, function (respSpace) {
      result['battery'] = []
      result['powerDistribution'] = []
      result['powerSource'] = []
      result['powerEquipment'] = []
      if (respSpace[0].battery.length > 0) {
        let batProps = []
        respSpace[0].battery.forEach((i) => {
          batProps.push({
            name: i.name,
            label: i.label,
            type: i.type,
            purpose: i.purpose,
            cell: i.totalCell,
            cellAV: `${i.amperePerCell}/${i.voltagePerCell}`,
            manufacturer: i.manufacturer,
            installDate: i.createdAt,
            links: {
              view: { url: '/battery/' + i._id + '?parentId=' + id, method: 'GET' },
              delete: { url: '/battery/' + i._id + '?parentId=' + id, method: 'DELETE' },
            }
          })
        })
        result['battery'] = batProps
      }
      if (respSpace[0].powerDistribution.length > 0) {
        let pdisProps = []
        respSpace[0].powerDistribution.forEach((i) => {
          pdisProps.push({
            name: i.name,
            label: i.label,
            type: i.type,
            purpose: i.purpose,
            output: "-",
            input: `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate: "-",
            links: {
              view: { url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'GET' },
              delete: { url: '/powerDistribution/' + i._id + '?parentId=' + id, method: 'DELETE' },
            }
          })
        })
        result['powerDistribution'] = pdisProps
      }
      if (respSpace[0].powerSource.length > 0) {
        let psProps = []
        respSpace[0].powerSource.forEach((i) => {
          psProps.push({
            name: i.name,
            label: i.label,
            type: i.type,
            purpose: i.purpose,
            output: "-",
            input: `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate: "-",
            links: {
              view: { url: '/powerSource/' + i._id + '?parentId=' + id, method: 'GET' },
              delete: { url: '/powerSource/' + i._id + '?parentId=' + id, method: 'DELETE' },
            }
          })
        })
        result['powerSource'] = psProps
      }
      if (respSpace[0].powerEquipment.length > 0) {
        let peProps = []
        respSpace[0].powerEquipment.forEach((i) => {
          peProps.push({
            name: i.name,
            label: i.label,
            type: i.type,
            purpose: i.purpose,
            output: "-",
            input: `${i.inputAmpere}/${i.inputVoltage}/${i.type}`,
            installDate: "-",
            links: {
              view: { url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'GET' },
              delete: { url: '/powerEquipment/' + i._id + '?parentId=' + id, method: 'DELETE' },
            }
          })
        })
        result['powerEquipment'] = peProps
      }
      res.render('location/details', {
        title: `${serverConfig.server.name.toUpperCase()}`,
        user: res.locals.user,
        data: result
      })
    })
  })
})

router.delete('/:_id', function (req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function () {
    res.status(200).send({
      links: {
        index: {
          url: '/location/',
          method: 'GET'
        }
      }
    })
  })
})
router.delete('/building/:buildingId/:relationshipId/:locationId', function (req, res, next) {
  const _id = req.params.buildingId
  const locationId = req.params.locationId
  const relationshipId = req.params.relationshipId
  internalService.deleteRelationship(relationshipId, res, next, function () {
    internalService.deleteNode(_id, res, next, function () {
      res.status(200).send({
        links: {
          index: {
            url: `/location/${locationId}`,
            method: 'GET'
          }
        }
      })
    })
  })
})
router.get('/:name/location', function (req, res, next) {
  const name = req.params.locationName
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Location',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: 'Building',
      relationLabel: "LOCATED_AT",
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    },]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }
      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }
      if (err.status !== 404) {
        result.error = err.message
      }
      res.status(200).send(result)
    }
  )
})
router.post('/building', function (req, res, next) {
  const reqData = {
    label: 'Building',
    properties: {
      name: req.body.name,
      type: req.body.type,
      height: req.body.height,
      width: req.body.width,
      length: req.body.lengthBuilding,
      squareMeter: req.body.squareMeter,
      labelingCode: req.body.labelingCode,
      floor: req.body.floor,
      builtDate: req.body.builtDate,
      status: 'ACTIVE'
    }
  }
  internalService.createNode(reqData, res, next, function (resData) {
    const paramsData = {
      startNodeId: resData.node._id,
      endNodeId: req.body.locationId,
      type: "LOCATED_AT"
    }
    internalService.createRelationship(paramsData, res, next, function (resData2) {
      let request = {
        collection: 'IDC-FE',
        type: 'QUERY',
        name: 'GENERATE_FLOOR',
        body: {
          params: {
            buildingId: parseInt(resData.node._id),
            floorcount: parseInt(req.body.floor),
            createdBy: res.locals.user
          }
        }
      }
      internalService.predefinedQuery(request, res, next, function (resData) {
        res.status(200).send({
          links: {
            view: {
              url: '/location/' + req.body.locationId,
              method: 'GET'
            }
          }
        })
      })
    })
  })
})

router.get('/:name/building', function (req, res, next) {
  const name = req.params.name
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Building',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {},
    relatedNodes: [{
      label: "Location",
      relationLabel: "LOCATED_AT",
      // directionSensitive: true,
      includeRelationship: true,
      collection: true,
      propertyFilter: {
        name: name
      }
    },]
  }

  if (queries.order) {
    queries.order.forEach(function (item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function (item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function (resData) {
      const data = []

      resData.nodes.forEach(function (item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function (err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})




// map handler




module.exports = router