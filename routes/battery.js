const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'type', label: 'Type', filter: {type: 'text'}},
  {name: 'label', label: 'Label', filter: {type: 'text'}},
  {name: 'manufacturer', label: 'Manufacturer', filter: {type: 'text'}},
  {name: 'model', label: 'Model', filter: {type: 'text'}},
  {name: 'totalCell', label: 'Total Cell', filter:{type: 'text'}},
  {name: 'voltagePerCell', label: 'Voltage Per Cell', filter:{type: 'text'}},
  {name: 'amperePerCell', label: 'Ampere Per Cell', filter:{type: 'text'}},
  {name: 'status', label: 'Status', filter:{type: 'text'}},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Battery',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            type: item.node.type ? item.node.type : null,
            label: item.node.label ? item.node.label : null,
            uid: item.node.uid ? item.node.uid : null,
            totalCell: item.node.totalCell ? item.node.totalCell : null,
            voltagePerCell: item.node.voltagePerCell ? item.node.voltagePerCell : null,
            amperePerCell: item.node.amperePerCell ? item.node.amperePerCell : null,
            manufacturer: item.node.manufacturer ? item.node.manufacturer : null,
            model: item.node.model ? item.node.model : null,
            status: item.node.status ? item.node.status: null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
            updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
            links: {
                view: {url: '/battery/' + item.node._id, method: 'GET'}
            }
        })
      })
      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('battery/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/battery/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['roomId'] = parseInt(req.body.roomId)
  params['ipOn'] = req.get('host')
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_BATTERY',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/battery/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Battery',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/battery/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
        _id: resData.node._id ? resData.node._id : null,
        name: resData.node.name ? resData.node.name : null,
        type: resData.node.type ? resData.node.type : null,
        label: resData.node.label ? resData.node.label : null,
        uid: resData.node.uid ? resData.node.uid : null,
        totalCell: resData.node.totalCell ? resData.node.totalCell : null,
        voltagePerCell: resData.node.voltagePerCell ? resData.node.voltagePerCell : null,
        amperePerCell: resData.node.amperePerCell ? resData.node.amperePerCell : null,
        manufacturer: resData.node.manufacturer ? resData.node.manufacturer : null,
        purpose: resData.node.purpose ? resData.node.purpose : null,
        model: resData.node.model ? resData.node.model : null,
        status: resData.node.status ? resData.node.status : null,
        createdAt: resData.node.createdAt ? resData.node.createdAt : null,
        createdBy: resData.node.createdBy ? resData.node.createdBy : null,
        updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
        updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
        links: {
            delete: {url: '/battery/' + resData.node._id + '?parentId=' + req.query.parentId, method: 'DELETE'},
            edit: {url: '/battery/' + resData.node._id, method: 'PUT'}
        }
    })

    res.render('battery/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  var data = {
    nodeId : _id
  }

  internalService.deleteNode(data, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/battery/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
