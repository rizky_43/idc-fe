const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'type', label: 'Location Type', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'name', label: 'Location Name', link: 'view', filter: {type: 'text'}},
  {name: 'uid', label: 'Site ID', filter: {type: 'text'}},
  {name: 'address', label: 'Address', type: 'text'},
  {name: 'geometry', label: 'Geometry', type: 'date'},
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Asset',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          uid: item.node.uid ? item.node.uid : null,
          name: item.node.name ? item.node.name : null,
          type: item.node.type ? item.node.type : null,
          address: item.node.address ? item.node.address : null,
          geometry: item.node.geometry ? item.node.geometry : null,
          status: item.node.status ? item.node.status : null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/asset/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('asset/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/asset/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'Asset',
    properties: {
      name: req.body.name,
      type: req.body.type,
      _id: req.body._id,
      address: req.body.address,
      geometry: req.body.geometry,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/asset/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Asset',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/asset/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    var item = resData
    data.push({
      _id: item.node._id ? item.node._id : null,
      uid: item.node.uid ? item.node.uid : null,
      name: item.node.name ? item.node.name : null,
      type: item.node.type ? item.node.type : null,
      address: item.node.address ? item.node.address : null,
      geometry: item.node.geometry ? item.node.geometry : null,
      status: item.node.status ? item.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      contactInfo:[],
      links: {
        delete: {url: '/asset/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/asset/' + resData.node._id, method: 'PUT'},
      },
    })

    res.render('asset/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/asset/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router

