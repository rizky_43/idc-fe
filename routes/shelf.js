const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'number', label: 'Shelf Number', filter: {type: 'text'}},
  {name: 'label', label: 'Label', filter: {type: 'text'}},
  {name: 'height', label: 'Height (cm)', filter:{type: 'text'}},
  {name: 'width', label: 'Width (cm)', filter:{type: 'text'}},
  {name: 'length', label: 'Length (cm)', filter:{type: 'text'}},
  {name: 'serialNumber', label: 'Serial Number', filter:{type: 'text'}},
  {name: 'status', label: 'Status', filter:{type: 'text'}},
  {name: 'createdAt', label: 'Created At', filter:{type: 'text'}},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'Shelf',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          name: item.node.name ? item.node.name : null,
          number: item.node.number ? item.node.number : null,
          label: item.node.label ? item.node.label : null,
          uid: item.node.uid ? item.node.uid : null,
          height: item.node.height ? item.node.height : null,
          width: item.node.width ? item.node.width : null,
          length: item.node.length ? item.node.length : null,
          serialNumber: item.node.serialNumber ? item.node.serialNumber : null,
          purpose: item.node.purpose ? item.node.purpose : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/shelf/' + item.node._id, method: 'GET'}
          },
          floor:[]
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('shelf/list', { title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/shelf/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  let params = req.body
  params['createdBy'] = res.locals.user
  params['rackId'] = parseInt(req.body.rackId)
  params['ipOn'] = req.get('host')
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'CREATE_SHELF',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/shelf/' + resData[0]._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'Shelf',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/shelf/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      name: resData.node.name ? resData.node.name : null,
      number: resData.node.number ? resData.node.number : null,
      label: resData.node.label ? resData.node.label : null,
      uid: resData.node.uid ? resData.node.uid : null,
      height: resData.node.height ? resData.node.height : null,
      width: resData.node.width ? resData.node.width : null,
      length: resData.node.length ? resData.node.length : null,
      serialNumber: resData.node.serialNumber ? resData.node.serialNumber : null,
      purpose: resData.node.purpose ? resData.node.purpose : null,
      status: resData.node.status ? resData.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/shelf/' + resData.node._id + '?parentId=' + req.query.parentId , method: 'DELETE'},
        edit: {url: '/shelf/' + resData.node._id, method: 'PUT'}
      },
      slotData: [],
      rackData: []
    })

    if(resData.relationships[0].relationship !== null){
      resData.relationships.forEach(function(item){
        if(item.node._labels.includes('Slot')){

          data[0].slotData.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            number: item.node.number ? item.node.number : null,
            label: item.node.label ? item.node.label : null,
            uid: item.node.uid ? item.node.uid : null,
            purpose: item.node.purpose ? item.node.purpose : null,
            status: item.node.status ? item.node.status: null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
            updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
            links: {
              view: {url: '/slot/' + item.node._id + '?parentId=' + resData.node._id, method : 'GET'},
              delete: {url: '/slot/' + item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
            }
          })
        }

        if(item.node._labels.includes('Rack')){
          
          data[0].rackData.push({
            _id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            type: item.node.type ? item.node.type : null,
            label: item.node.label ? item.node.label : null,
            uid: item.node.uid ? item.node.uid : null,
            purpose: item.node.purpose ? item.node.purpose : null,
            status: item.node.status ? item.node.status: null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
            updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
            links: {
              view: {url: '/slot/' + item.node._id + '?parentId=' + resData.node._id, method : 'GET'},
              delete: {url: '/slot/' + item.node._id + '?parentId=' + resData.node._id, method: 'DELETE'},
            }
          })
        }
      })
    }

    res.render('shelf/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  var data = {
    nodeId : _id,
    otherNodeIds : []
  }

  data.otherNodeIds.push(req.query.parentId)

  internalService.deleteNodeWithEdges(data, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/space/',
          method: 'GET'
        }
      }
    })
  })
})

router.get('/:id/generateSlot', function(req, res, next) {
  var type = req.query.rackType
  var count = 0
  if (type == 'FULL-RACK') {
    count = 45
  } else if(type=='SUB-RACK'){
    count = 15
  }
  let params = {}
  params['createdBy'] = res.locals.user
  params['shelfId'] = parseInt(req.params.id)
  params['ipOn'] = req.get('host')
  params['slotCount'] = count
  let request = {
    collection : 'IDC-FE' ,
    type : 'QUERY',
    name : 'GENERATE_SLOT',
    body : {
      params : params
    }
  }
  internalService.predefinedQuery(request, res, next, function(resData) {
    res.status(200).send()
  })
})

module.exports = router
