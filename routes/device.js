const express = require('express')
const router = express.Router()
const log = serverUtils.getLogger('routes.device')

const utils = require('../lib/Utils')
const security = require('../lib/Security')
const internalService = require('../lib/ISPService')

const dataStructure = [
  {name: 'name', label: 'Name', link: 'view', filter: {type: 'text'}},
  {name: 'ipAddress', label: 'IP Address', filter: {type: 'text'}},
  {name: 'type', label: 'Type', filter: {type: 'text', url: '/device/type'}},
  {name: 'networkRole', label: 'Network Role', filter: {type: 'text'}, type: 'network-role'},
  {name: 'status', label: 'Status', filter: {type: 'text'}, type: 'status'},
  {name: 'createdAt', label: 'Created At', type: 'date'},
  {name: 'createdBy', label: 'Created By', filter: {type: 'text'}}
]

router.get('/type', function(req, res, next) {
  // security.protect('DEVICE', req, res, next, function() {
    internalService.getResourceType(res, next, function(responseData) {
      const result = []
      responseData.forEach(function(item) {
        result.push({
          name: item
        })
      })
      res.send(result)
    })
  // })
})

// GET JSON list of device
router.get('/data', function(req, res, next) {
  // security.protect('DEVICE', req, res, next, function() {
    const queries = req.query
    log.debug(queries)
    const requestData = {
      limit: queries.length ? queries.length : 10,
      label: 'Resource',
      skip: queries.start,
      propertyFilter: {},
      propertySort: {}
    }

    if (queries.order) {
      queries.order.forEach(function(item) {
        const field = dataStructure[item.column]
        if (field) {
          requestData.propertySort[field.name] = item.dir
        }
      })
    }

    if (queries.columns) {
      queries.columns.forEach(function(item) {
        if (item.search.value !== '' && item.name) {
          requestData.propertyFilter[item.name] =
            '(?i)' +
            item.search.value
              .replace(/ /g, '*')
              .replace(/\(/g, '\\(')
              .replace(/\)/g, '\\)') +
            '*'
        }
      })
    }

    internalService.listNodes(
      requestData,
      res,
      next,
      function(responseData) {
        const data = []

        responseData.nodes.forEach(function(item) {
          data.push({
            id: item.node._id ? item.node._id : null,
            name: item.node.name ? item.node.name : null,
            label: item.node.label ? item.node.label : null,
            ipAddress: item.node.ipAddress ? item.node.ipAddress : null,
            type: item.node.type ? item.node.type : null,
            networkRole: item.node.networkRole ? item.node.networkRole : null,
            status: item.node.status ? item.node.status : null,
            createdAt: item.node.createdAt ? item.node.createdAt : null,
            createdBy: item.node.createdBy ? item.node.createdBy : null,
            links: {
              view: {url: '/device/' + item.node._id, method: 'GET'}
            }
          })
        })

        const result = {
          draw: queries.draw,
          data: data,
          recordsFiltered: responseData.pager.total,
          recordsTotal: responseData.pager.total
        }

        res.send(result)
      },
      function(err) {
        const result = {
          draw: queries.draw,
          data: [],
          recordsFiltered: 0,
          recordsTotal: 0
        }

        if (err.status !== 404) {
          result.error = err.message
        }

        res.status(200).send(result)
      }
    )
  // })
})

router.get('/', function(req, res, next) {
  // security.protect('DEVICE', req, res, next, function() {
    res.render('device/list', {
      user: res.locals.user,
      dataStructure: dataStructure,
      links: {
        data: {url: '/device/data', method: 'GET'}
      }
    })
  // })
})

router.post('/', function(req, res, next) {
  // security.protectWithAttribute('DEVICE', req.body, req, res, next, function() {
    const requestData = {
      label: 'Resource',
      properties: {
        name: req.body.name,
        label: req.body.name,
        ipAddress: req.body.ipAddress,
        type: req.body.type,
        networkRole: req.body.networkRole,
        status: 'ACTIVE'
      }
    }

    internalService.createNode(requestData, res, next, function(responseData) {
      const deviceId = responseData.node._id
      const relationData = {
        startNodeId: deviceId,
        endNodeId: req.body.manufactureId,
        type: 'MANUFACTURED_BY',
        properties: {
          model: req.body.model
        }
      }

      internalService.createRelationship(relationData, res, next, function() {
        res.status(200).send({
          id: deviceId,
          links: {
            view: {url: '/device/' + deviceId, method: 'GET'}
          }
        })
      })
    })
  // })
})

// GET device details
router.get('/:deviceId', function(req, res, next) {
  const deviceId = req.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    internalService.getNode(deviceId, true, res, next, function(responseData) {
      const data = {
        id: responseData.node._id,
        name: responseData.node.name,
        ipAddress: responseData.node.ipAddress,
        label: responseData.node.label,
        model: responseData.node.model,
        type: responseData.node.type,
        rejected: responseData.node.rejected,
        entityId: responseData.node.entityId,
        idSecondary: responseData.node.id,
        networkRole: responseData.node.networkRole,
        installedDate: responseData.node.installedDate,
        createdAt: utils.formatDate(responseData.node.createdAt),
        createdBy: responseData.node.createdBy,
        source: responseData.node.source,
        maxVLAN: responseData.node.maxVLAN,
        status: responseData.node.status,
        nat: responseData.node.nat,
        manufacture: null,
        location: null,
        map : [],
        ports: [],
        sdpRelations: [],
        managedServices: [],
        provisionedServices: [],
        linkedDevices: [],
        modules: [],
        apGroup: [],
        service: [],
        vlan: [],
        ssid: [],
        others: [],
        associatedVrf: [],
        regional: {},
        links: {
          view: {url: '/device/' + responseData.node._id, method: 'GET'},
          edit: {
            url: '/device/' + responseData.node._id + '?networkRole=' + responseData.node.networkRole,
            method: 'PUT'
          },
          delete: {
            url: '/device/' + responseData.node._id + '?networkRole=' + responseData.node.networkRole,
            method: 'DELETE'
          },
          addPort: {
            url: '/device/' + responseData.node._id + '/port' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          addVLAN: {
            url: '/device/' + responseData.node._id + '/vlan' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          vlanData: {
            url: `/device/${responseData.node._id}/vlan`,
            method: 'GET'
          },
          addVrf: {
            url: '/device/relation/vrf' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          addPop: {url: '/device/pop' + '?networkRole=' + responseData.node.networkRole, method: 'POST'},
          addServiceType: {
            url: '/device/serviceType' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          addManufacture: {
            url: '/device/' + responseData.node._id + '/manufacture' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          addModule: {
            url: '/device/' + responseData.node._id + '/module' + '?networkRole=' + responseData.node.networkRole,
            method: 'POST'
          },
          vrfData: {
            url: '/vrf/device/' + responseData.node._id,
            method: 'GET'
          },
          queryVrf: {
            url: '/vrf/data',
            method: 'GET'
          },
          adhocSync: {
            url: '/sync/portAdhocRequest',
            method: 'POST',
            data: {
              id: responseData.node.ipAddress
            }
          }
        }
      }

      responseData.relationships.forEach(function(item) {
        if (!item.relationship) {
          return
        }

        if (item.relationship._type === 'MANUFACTURED_BY') {
          data.manufacture = {
            id: item.node._id,
            name: item.node.name,
            model: item.relationship.model,
            relationId: item.relationship._id,
            maxVLAN: item.node.maxVLAN,
            links: {
              delete: {
                url:
                  '/device/' +
                  responseData.node._id +
                  '/connection/' +
                  item.relationship._id +
                  '?networkRole=' +
                  responseData.node.networkRole,
                method: 'DELETE'
              }
            }
          }
        } else if (item.relationship._type === 'LOCATED_AT') {
          data.location = item.node.name
          data.geometry = item.node.geometry
          data.map.push({
            coordinate : item.node.geometry,
            properties : {
              name :  item.node.name
            }
          })
        } else if (item.relationship._type === 'PROVISION_IN') {
          data.provisionedServices.push({
            id: item.node._id,
            name: item.node.name,
            links: {
              view: {url: '/serviceType/' + item.node._id, method: 'GET'},
              delete: {
                url:
                  '/device/' +
                  responseData.node._id +
                  '/connection/' +
                  item.relationship._id +
                  '?networkRole=' +
                  responseData.node.networkRole,
                method: 'DELETE'
              }
            }
          })
        } else if (item.relationship._type === 'MANAGED_IN') {
          data.managedServices.push({
            serviceType: item.relationship.serviceType,
            role: item.relationship.role,
            name: item.node.name,
            links: {
              popView: {url: '/area/pop/' + item.node._id, method: 'GET'},
              delete: {
                url:
                  '/device/' +
                  responseData.node._id +
                  '/connection/' +
                  item.relationship._id +
                  '?networkRole=' +
                  responseData.node.networkRole,
                method: 'DELETE'
              }
            }
          })
        } else if (item.relationship._type === 'LINKED_TO') {
          data.linkedDevices.push({
            id: item.node._id,
            name: item.node.name,
            label: item.node.label,
            ipAddress: item.node.ipAddress,
            networkRole: item.node.networkRole,
            type: item.node.type,
            relationName: item.relationship.name,
            relationId: item.relationship._id,
            relationship: item.relationship,
            links: {
              view: {url: '/device/' + item.node._id, method: 'GET'},
              delete: {
                url:
                  '/device/' +
                  responseData.node._id +
                  '/connection/' +
                  item.relationship._id +
                  '?networkRole=' +
                  responseData.node.networkRole,
                method: 'DELETE'
              }
            }
          })
        } else if (item.relationship._type === 'HAS_MEMBER' && item.node._labels.includes('Module')) {
          let module = {
            id: item.node._id,
            name: item.node.name,
            ipAddress: item.node.ipAddress,
            nat: item.node.nat,
            hostname: item.node.hostname,
            type: item.node.type,
            model: item.node.model,
            serialNumber: item.node.serialNumber,
            status: item.node.status,
            syncData: item.node.syncData,
            createdAt: utils.formatDate(item.node.createdAt),
            createdBy: item.node.createdBy,
            relationId: item.relationship._id,
            links: {
              view: {
                url: '/device/' + responseData.node._id + '/module/' + item.node._id,
                method: 'GET'
              },
              syncSet: {
                url: `/sync/set?id=${item.node._id}&label=Module`,
                method: 'GET'
              },
              syncUnset: {
                url: `/sync/unset?id=${item.node._id}&label=Module`,
                method: 'GET'
              },
              delete: {
                url:
                  '/device/' +
                  responseData.node._id +
                  '/connection/' +
                  item.relationship._id +
                  '?networkRole=' +
                  responseData.node.networkRole,
                method: 'DELETE'
              }
            }
          }

          if (data.regional && data.regional.name) module.links.view.url += '?reg=' + data.regional.name

          data.modules.push(module)
        } else if (item.relationship._type === 'DEPLOYED_IN') {
          data.regional = item.node
          if (data.modules && data.modules.length > 0) {
            data.modules.forEach(module => {
              module.links.view.url += '?reg=' + item.node.name
            })
          }
        } else if (item.relationship._type === 'HAS_VLAN') {
          true
        } else if (item.relationship._type === 'GROUPED_IN') {
          data.apGroup.push({
            name: item.node.name,
            relationId: item.relationship._id,
            links: {
              view: {
                url: '/apGroup/' + item.node._id,
                method: 'GET'
              }
            }
          })
        } else if (item.relationship._type === 'SERVING' || item.relationship._type === 'SERVING_TO') {
          data.service.push({
            serviceId: item.node.sid,
            reservationId: item.node.reservationId,
            relationId: item.relationship._id,
            links: {
              view: {
                url: '/serviceOrder/' + item.node._id,
                method: 'GET'
              }
            }
          })
        } else if (item.relationship._type === 'HAS_SSID') {
          data.ssid.push({
            name: item.node.name,
            links: {
              view: {
                url: '/ssid/' + item.node._id,
                method: 'GET'
              }
            }
          })
        } else if (item.relationship._type === 'WITH_VRF') {
          true
        } else if (item.relationship._type !== 'HAS_PORT' && item.relationship._type !== 'SDP') {
          data.others.push({
            relationshipPayload: item.relationship,
            nodePayload: item.node
          })
        }
      })

      data.links.ports = {
        url:
          '/device/' +
          responseData.node._id +
          '/port?maxVlan=' +
          (data.maxVLAN
            ? data.maxVLAN
            : data.manufacture
              ? data.manufacture.maxVLAN
                ? data.manufacture.maxVLAN
                : -1
              : -1),
        method: 'GET'
      }

      let page = 'device/details'
      // if (data.networkRole === 'WAC') {
      //   page = 'device/wac_details'
      // } else if (data.networkRole === 'WAG') {
      //   page = 'device/wag_details'
      // } else if (data.networkRole === 'AN') {
      //   page = 'device/an_details'
      // } else if (data.networkRole === 'ME') {
      //   page = 'device/me_details'
      // } else if (data.networkRole === 'AP') {
      //   page = 'device/ap_details'
      // }

      res.render(page, {
        user: res.locals.user,
        data: data
      })
    })
  // })
})

router.put('/:nodeId', function(req, res, next) {
  const nodeId = req.params.nodeId

  // security.protectWithFetchDevice(nodeId, 'DEVICE', req, res, next, function() {
    const requestData = {
      label: 'Resource',
      id: nodeId,
      properties: req.body
    }

    internalService.updateNode(requestData, res, next, function() {
      res.status(204).send()
    })
  // })
})

router.delete('/:nodeId', function(req, res, next) {
  const nodeId = req.params.nodeId

  // security.protectWithFetchDevice(nodeId, 'DEVICE', req, res, next, function() {
    internalService.deleteNode(nodeId, res, next, function() {
      res.status(200).send({
        id: nodeId,
        links: {
          index: {
            url: '/device',
            method: 'GET'
          }
        }
      })
    })
  // })
})

router.get('/:deviceId/vrf', function(req, res, next) {
  log.debug('[/:deviceId/vrf] params %O', req.params)
  // security.protect('DEVICE', req, res, next, function() {
    const requestData = {
      label: 'Resource',
      propertyFilter: {
        _id: req.params.deviceId
      },
      relatedNodes: [
        {
          includeRelationship: false,
          label: 'VRF'
        }
      ]
    }

    internalService.listNodes(requestData, res, next, function(responseData) {
      const data = []
      responseData.nodes.forEach(function(item) {
        data.push({
          id: item.vrf._id,
          name: item.vrf.name,
          maxRoutes: item.vrf.maxRoutes
        })
      })
      res.status(200).send(data)
    })
  // })
})

router.delete('/relation/vrf', function(req, res, next) {
  const vrfName = req.body.vrfName
  const deviceId = req.body.deviceId
  const deviceName = req.body.deviceName

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    internalService.deleteVrfDeviceAssoc(vrfName, deviceName, res, next, function() {
      res.status(200).send({
        id: deviceId,
        links: {
          view: {url: '/device/' + deviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.post('/relation/vrf', function(req, res, next) {
  const deviceId = req.body.deviceId
  const vrfId = req.body.vrfId
  const rd = req.body.rd

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    const createData = {
      label: 'RD',
      merge: true,
      properties: {
        name: rd
      }
    }

    internalService.createNode(createData, res, next, function(responseData) {
      const relationData = {
        startNodeId: responseData.node._id,
        endNodeId: vrfId,
        type: 'BELONGS_TO'
      }

      internalService.createRelationship(relationData, res, next, function() {
        const relationData2 = {
          startNodeId: deviceId,
          endNodeId: vrfId,
          type: 'WITH_VRF',
          properties: {
            RD: rd
          }
        }

        internalService.createRelationship(relationData2, res, next, function() {
          res.redirect('/device/' + deviceId)
        })
      })
    })
  // })
})

router.put('/:deviceId/manufacture', function(req, res, next) {
  const deviceId = req.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    log.mark(req.body)

    const data = {
      startNodeId: deviceId,
      endNodeId: req.body.manufactureId,
      id: req.body.relationId,
    }
    internalService.moveRelationship(data, res, next, function(resp) {
      const relationData = {
        startNode: resp.rel._startNodeId,
        endNode: resp.rel._endNodeId,
        type: 'MANUFACTURED_BY',
        id: resp.rel._id,
        properties: {
          model: req.body.model
        }
      }
      internalService.updateRelationship(relationData, res, next, function() {
        res.redirect('/device/' + deviceId)
      })
    })
  // })
})

router.post('/:deviceId/manufacture', function(req, res, next) {
  const deviceId = req.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    const relationData = {
      startNodeId: deviceId,
      endNodeId: req.body.manufactureId,
      type: 'MANUFACTURED_BY',
      properties: {
        model: req.body.model
      }
    }

    internalService.createRelationship(relationData, res, next, function() {
      res.status(200).send({
        id: deviceId,
        links: {
          view: {url: '/device/' + deviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.post('/:deviceId/connection', function(req, res, next) {
  const deviceId = req.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    const relationData = {
      startNodeId: deviceId,
      endNodeId: req.body.targetNodeId,
      type: 'LINKED_TO',
      properties: {
        name: req.body.relationName
      }
    }

    internalService.createRelationship(relationData, res, next, function() {
      res.status(200).send({
        id: deviceId,
        links: {
          view: {url: '/device/' + deviceId, method: 'GET'}
        }
      })
    })
  // })
})

router.delete('/:deviceId/connection/:relationshipId', function(req, res, next) {
  const relationshipId = req.params.relationshipId
  const deviceId = req.params.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    internalService.deleteRelationship(relationshipId, res, next, function() {
      res.status(200).send({
        id: deviceId,
        links: {
          index: {
            url: '/device/' + deviceId,
            method: 'GET'
          }
        }
      })
    })
  // })
})

router.post('/pop', function(req, res, next) {
  const serviceParam = req.body.serviceTypeIdName.split('_')
  const serviceTypeId = serviceParam[0]
  const serviceTypeName = serviceParam[1]
  const deviceId = req.body.deviceId
  const popId = req.body.popId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    const relationData1 = {
      startNodeId: popId,
      endNodeId: serviceTypeId,
      type: 'SERVING'
    }

    internalService.createRelationship(relationData1, res, next, function() {
      const relationData2 = {
        startNodeId: deviceId,
        endNodeId: popId,
        type: 'MANAGED_IN',
        properties: {
          role: req.body.role
        },
        boundProperties: {
          serviceType: serviceTypeName
        }
      }

      internalService.createRelationship(relationData2, res, next, function() {
        const relationData3 = {
          startNodeId: serviceTypeId,
          endNodeId: deviceId,
          type: 'PROVISION_IN'
        }

        internalService.createRelationship(relationData3, res, next, function() {
          res.status(200).send({
            links: {
              view: {url: '/device/' + deviceId, method: 'GET'}
            }
          })
        })
      })
    })
  // })
})

router.post('/serviceType', function(req, res, next) {
  const deviceId = req.body.deviceId

  // security.protectWithFetchDevice(deviceId, 'DEVICE', req, res, next, function() {
    const relationData = {
      startNodeId: req.body.serviceTypeId,
      endNodeId: deviceId,
      type: 'PROVISION_IN'
    }

    internalService.createRelationship(relationData, res, next, function() {
      res.status(204).send()
    })
  // })
})

module.exports = router
