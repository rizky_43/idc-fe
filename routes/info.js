const express = require('express');
const router = express.Router();
const Backbone = require('../lib/BackboneService');
const sendRequest = Backbone.sendRequest;

router.get('/findOpticalEntities', function (clientRequest, clientResponse, next) {
    var requestData = {
        "cql": "MATCH (o:OpticalEntities{type:'OMS'}) WHERE o.name starts with $name RETURN {name:o.name,id:toFloat(id(o))} limit 6",
        "params": {
            "name":clientRequest.query.value.toUpperCase()
        }
      };
    sendRequest('admin', '/refferenceObjects/selfquery',requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
                result.push({
                    name: item.name,
                    id:item.id
                });
        });
        clientResponse.send(result);
    });
});


router.get('/spesification', function (clientRequest, clientResponse, next) {
    const requestData = {
        name: "GET_PhysicalDevice_Specification",
        params: {
          type: "PhysicalDevice_Specification"
        }
      }
    sendRequest('admin', '/refferenceObjects/boilerPlate', requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});
router.get('/pysicaldevicetype', function (clientRequest, clientResponse, next) {
    const requestData = {
        name: "GET_PhysicalDevice_Type",
        params: {
          type: "PhysicalDevice_Type"
        }
      }
    sendRequest('admin', '/refferenceObjects/boilerPlate', requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});
router.get('/activemodules', function (clientRequest, clientResponse, next) {
    const requestData = {
        name: "GET_Active_Modules",
        params: {
            devicetype: clientRequest.query.devicetype
        }
      }
    sendRequest('admin', '/refferenceObjects/boilerPlate', requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item
            });
        });
        clientResponse.send(result);
    });
});
router.get('/physicaldevice', function (clientRequest, clientResponse, next) {
    var requestData = {
        "cql": "MATCH (s:Specification) WHERE s.type = $type RETURN s.name as name",
        "params": {
            "type" : "Module_Template"
        }
      };
    sendRequest('admin', '/refferenceObjects/selfquery',requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
                result.push({
                    name: item
                });
        });
        clientResponse.send(result);
    });
});
router.get('/locn', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/location/find?name='+ clientRequest.query.locnQuery.toUpperCase() +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            data.push({
              name: item.name ? item.name : null
            });
        });
        clientResponse.send(data);
    });
});
router.get('/sto', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=STO&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                areacode: item.areacode
            });
        });
        clientResponse.send(result);
    });
});
router.get('/type', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=CableSheathType&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.type,
                key: item.key
            });
        });
        clientResponse.send(result);
    });
});
router.get('/locationType', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=LocationTypes&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});
router.get('/manufacturer', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=Manufacturer&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});
router.get('/productName', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=CableSheathProductType&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name,
                key : item.legacy_id
            });
        });
        clientResponse.send(result);
    });
});
router.get('/finddevice', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label='+clientRequest.query.label+'&page=1&rows=6&filter='+ clientRequest.query.phyquery +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            data.push({
              name: item.name ? item.name : null
            });
        });
        clientResponse.send(data);
    });
});
router.get('/findphysicaldevice', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=PhysicalDevice&page=1&rows=20&filter='+ clientRequest.query.phyquery +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            data.push({
              name: item.name ? item.name : null
            });
        });
        clientResponse.send(data);
    });
});
router.get('/findradio', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=PhysicalDevice&page=1&rows=6&filter='+ clientRequest.query.phyquery +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            if(item.networkRole == "RADIO"){
                data.push({
                    name: item.name ? item.name : null
                  });
            }
        });
        clientResponse.send(data);
    });
});
router.get('/findCableSheath', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=CableSheath&page=1&rows=6&filter='+ clientRequest.query.cablesheath +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            data.push({
              name: item.name ? item.name : null
            });
        });
        clientResponse.send(data);
    });
});
router.get('/dynamicobjectinfo', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label='+clientRequest.query.label+'&page=1&rows=6&filter='+ clientRequest.query.value +'&operation=startwith', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData.forEach(function(item, index){
            data.push({
              name: item.name ? item.name : null
            });
        });
        clientResponse.send(data);
    });
});
router.get('/startport', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/physicalDevice/find?name='+ clientRequest.query.physicaldevice, null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function (item, index){
            if(item.module.name.includes('CASSETTE'))
                item.ports.forEach(function (port, index){
                    if(port.connection.length == 0)
                        data.push({
                            name: item.module.name+"-"+port.name
                        });
                });
        });
        clientResponse.send(data);
    });
});
router.get('/moduleInterface', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/physicalDevice/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            item.ports.forEach(function(item2,index){
                if(item2.connection.length < 2){
                    data.push({
                        name: item2.name+"@"+item.module.name
                      });
                }
            });
        });
        clientResponse.send(data);
    });
});
router.get('/moduleport', function (clientRequest, clientResponse, next) {
    var valType = 'physicalDevice';
    if(clientRequest.query.label){
        valType = clientRequest.query.label;
    }
    sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
     
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(valType == 'Resource'){
                data.push({
                    name: item.name+"|"+item.specification,
                    id:item.id ? item.id : null
                });
            }else{
                data.push({
                    name: item.module.name
                });
            }
        });
        clientResponse.send(data);
    });
});
router.get('/InterfacefromModule', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/physicalDevice/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(item.module.name == clientRequest.query.interface){
                if(item.module.name.includes("CASSETTE")){
                    item.ports.forEach(function(port,index){
                        if(port.connection.length === 0){
                            data.push({
                                name: port.name
                            });
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }
            }
        });
        clientResponse.send(data);
    });
});
router.get('/InterfacefromModuleup', function (clientRequest, clientResponse, next) {
    var valType = 'physicalDevice';
    if(clientRequest.query.label){
        valType = clientRequest.query.label;
    }
    sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(item.module.name == clientRequest.query.interface){
                if(item.module.name.includes("SPL")){
                    item.ports.forEach(function(item2,index){
                        if(item2.connection.length == 0){
                            data.push({
                                name: item2.name
                            });
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }else if(new RegExp("(RADIO_UNIT|ANTENNA_MODULE)").test(item.module.networkRole)){
                    item.ports.forEach(function(item2,index){
                        if(item2.name == "BAND"){
                            data.push({
                                name: item2.name
                            });
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }else{
                    item.ports.forEach(function(item2,index){
                        if(item2.connection.length < 2 & item2.connection.length > 0){
                            item2.connection.forEach(function(x,index){
                                if(x.direction == "DOWN"){
                                    data.push({
                                        name: item2.name
                                    });
                                }
                            }) 
                        }else if(item2.connection.length == 0){
                            data.push({
                                name: item2.name
                            });
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }
            }
        });
        clientResponse.send(data);
    });
});
router.get('/InterfacefromModuledown', function (clientRequest, clientResponse, next) {
    var valType = 'physicalDevice';
    if(clientRequest.query.label){
        valType = clientRequest.query.label;
    }
    sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(item.module.name == clientRequest.query.interface){
                if(item.module.name.includes("SPL")){
                    item.ports.forEach(function(item2,index){
                        if(item2.connection.length == 0){
                            data.push({
                                name: item2.name
                            });
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }else if(new RegExp("(RADIO_UNIT|ANTENNA_MODULE)").test(item.module.networkRole)){
                    item.ports.forEach(function(item2,index){
                        if(item2.name == "BAND"){
                            data.push({
                                name: item2.name
                            });
                        }
                    });
                // }else if(new RegExp("^((?!SLOT1|NA).)*$").test(item.module.name)){
                //     item.ports.forEach(function(item2,index){
                //         if(item2.connection.length == 0){
                //             data.push({
                //                 name: item2.name
                //             });
                //         }
                        
                //     });
                //     if(data.length === 0){
                //         data.push({
                //             name: ''
                //         });
                //     }
                }else{
                    item.ports.forEach(function(item2,index){
                        if(item2.connection.length < 2 & item2.connection.length > 0){
                            item2.connection.forEach(function(x,index){
                                if(x.direction == "UP" || x.direction == "JUMPER"){
                                    data.push({
                                        name: item2.name
                                    });
                                }
                            }) 
                        }else if(item2.connection.length == 0){
                            data.push({
                                name: item2.name
                            });
                        }
                        
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }
            }
        });
        clientResponse.send(data);
    });
});
router.get('/Interfaceleft', function (clientRequest, clientResponse, next) {
    var valType = 'physicalDevice';
    if(clientRequest.query.label){
        valType = clientRequest.query.label;
    }
    sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(valType == 'Resource'){   
                if(item.name+"|"+item.specification == clientRequest.query.interface){
                    item.ports.forEach(function(item2,index){
                        var conndition = clientRequest.query.channel ? clientRequest.query.channel : false;
                        if(conndition){
                            if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }else if(item2.connection.length == 1 && item2.connection[0].type !== 'OpticalEntities'){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }
                        }else{
                            if(item2.connection.length < 2){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }
            }else{
                if(item.module.name == clientRequest.query.interface){
                    if(item.module.name.includes("SPL")){
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }else{
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length < 2 & item2.connection.length > 0){
                                item2.connection.forEach(function(x,index){
                                    if(x.direction == "DOWN"){
                                        data.push({
                                            name: item2.name
                                        });
                                    }
                                }) 
                            }else if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                            
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }
                }
            }
        });
        clientResponse.send(data);
    });
});
router.get('/Interfaceright', function (clientRequest, clientResponse, next) {
    var valType = 'physicalDevice';
    if(clientRequest.query.label){
        valType = clientRequest.query.label;
    }
    sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
        const data = [];
        responseData[0].modules.forEach(function(item, index){
            if(valType == 'Resource'){   
                if(item.name+"|"+item.specification == clientRequest.query.interface){
                    item.ports.forEach(function(item2,index){
                        var conndition = clientRequest.query.channel ? clientRequest.query.channel : false;
                        if(conndition){
                            if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }else if(item2.connection.length == 1 && item2.connection[0].type !== 'OpticalEntities'){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }
                        }else{
                            if(item2.connection.length < 2){
                                data.push({
                                    name: item2.name,
                                    id: item2.id ? item2.id : null
                                });
                            }
                        }
                    });
                    if(data.length === 0){
                        data.push({
                            name: ''
                        });
                    }
                }
            }else{
                if(item.module.name == clientRequest.query.interface){
                    if(item.module.name.includes("SPL")){
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }else{
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length < 2 & item2.connection.length > 0){
                                item2.connection.forEach(function(x,index){
                                    if(x.direction == "UP" || x.direction == "JUMPER"){
                                        data.push({
                                            name: item2.name
                                        });
                                    }
                                }) 
                            }else if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                            
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }
                }
            }
        });
        clientResponse.send(data);
    });
});
router.get('/subinterface', function (clientRequest, clientResponse, next) {
    var valType = clientRequest.query.label ? clientRequest.query.label : 'physicalDevice';
    var phy = clientRequest.query.phy;
    var modul = clientRequest.query.modul;
    var interface = clientRequest.query.interface;
    if(valType=='Resource'){
        sendRequest('admin', '/'+ valType +'/findInterface?device_name='+phy+'&module_name='+modul+'&port_name='+interface, null,  'GET', clientResponse, function (responseData) {    
            const data = [];
            var obj = responseData[0].ports[0] ? responseData[0].ports[0].subInterfaces : [];
            obj.forEach(function(item, index){
                if(item.connection.length < 2){
                    data.push({
                        name: item.name
                    });
                }
            });
            if(data.length === 0){
                data.push({
                    name: ''
                });
            }
            clientResponse.send(data);
        });
    }else{
        sendRequest('admin', '/'+ valType +'/find?name='+ clientRequest.query.phy +'', null,  'GET', clientResponse, function (responseData) {
            const data = [];
            responseData[0].modules.forEach(function(item, index){
                if(item.module.name == clientRequest.query.interface){
                    if(item.module.name.includes("SPL")){
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }else{
                        item.ports.forEach(function(item2,index){
                            if(item2.connection.length < 2 & item2.connection.length > 0){
                                item2.connection.forEach(function(x,index){
                                    if(x.direction == "UP" || x.direction == "JUMPER"){
                                        data.push({
                                            name: item2.name
                                        });
                                    }
                                }) 
                            }else if(item2.connection.length == 0){
                                data.push({
                                    name: item2.name
                                });
                            }
                            
                        });
                        if(data.length === 0){
                            data.push({
                                name: ''
                            });
                        }
                    }
                }
            });
            clientResponse.send(data);
        });
    }
});
router.get('/coreavailable', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/cablesheath/findCores?name='+ clientRequest.query.cablesheathname +'', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            // item.xconnect.forEach(function (item2, index){
                // if(item2.direction == null){
                if(item.xconnect.length < 2){
                    result.push({
                        core: item.core
                    });
                }
            // });
            
        });
        clientResponse.send(result);
    });
});
router.get('/xconnectcoreavailable', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/cablesheath/findCores?name='+ clientRequest.query.cablesheathname +'', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            item.xconnect.forEach(function (item2, index){
                if(item2.direction == null){
                    result.push({
                        name: item.core
                    });
                }
            });
            
        });
        clientResponse.send(result);
    });
});
router.get('/regional', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/refferenceObjects/info?label=Regional&page=1&rows=2000', null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});
router.get('/networkRole', function (clientRequest, clientResponse, next) {
    var requestData = {
        "cql": "match (s:Specification) where s.type = 'networkRole' return s.name as name",
        "params": null
      };
    sendRequest('admin', '/refferenceObjects/selfquery',requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
                result.push({
                    name: item
                });
        });
        clientResponse.send(result);
    });
});
router.get('/dynamicspec', function (clientRequest, clientResponse, next) {
    var requestData = {
        "cql": "match (s:Specification) where s.type = $type return s.name as name",
        "params": {
            "type": clientRequest.query.label
        }
      };
    sendRequest('admin', '/refferenceObjects/selfquery',requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
                result.push({
                    name: item
                });
        });
        clientResponse.send(result);
    });
});
router.get('/getSto', function (clientRequest, clientResponse, next) {
    sendRequest('admin', '/area/stoByRegion?regional='+ clientRequest.query.phy, null, 'GET', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.areacode + " ["+item.shortcode+"]"
            });
        });
        clientResponse.send(result);
    });
});

router.get('/getStoWithParam/:reg', function (clientRequest, clientResponse, next) {
    var requestData = {
        "cql": "match (s:STO)-[:PART_OF]->(:Datel)-[:PART_OF]->(:Witel)-[:PART_OF]->(r:Regional) where r.name = $regional return {areacode: s.areacode, shortcode: s.description} as result",
        "params": {
          "regional": Buffer.from(clientRequest.params.reg, 'base64').toString()
        }
      };
    sendRequest('admin', '/refferenceObjects/selfquery',requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                label: item.areacode + " ["+item.shortcode+"]",
                name: item.areacode
            });
        });
        clientResponse.send(result);
    });
});

router.get('/status', function (clientRequest, clientResponse, next) {
    var obj = [{name:"INSERVICE"},{name:"OUTSERVICE"},{name:"SPARE"},{name:"PLANNED"}];
    clientResponse.send(obj);
});

router.get('/statusPhysicalDevice', function (clientRequest, clientResponse, next) {
    const requestData = {
        name: "GET_PhysicalDevice_Status",
        params: {
          type: "Status"
        }
      }
    sendRequest('admin', '/refferenceObjects/boilerPlate', requestData, 'PUT', clientResponse, function (responseData) {
        const result = [];
        responseData.forEach(function(item, index){
            result.push({
                name: item.name
            });
        });
        clientResponse.send(result);
    });
});

router.get('/specificationCostume', function (clientRequest, clientResponse, next) {
    const result = [];
    var obj = [
        {
            name: "FTM"
        },
        {
            name: "CABINET"
        },
        {
            name: "ODF"
        },
        {
            name: "ODP"
        },
        {
            name: "ODC"
        },
        {
            name: "CLOSURE"
        },
        {
            name: "OTB"
        },
        {
            name: "TLS"
        },
        {
            name: "CLS"
        },
        {
            name: "BMH"
        },
        {
            name: "RADIO_ANTENNA"
        }
    ];
    obj.forEach(function(item, index){
        result.push({
            name: item.name
        });
    });
    clientResponse.send(result);
});

router.get('/cableNetworkRole', function (clientRequest, clientResponse, next) {
    const result = [];
    var obj = ['BACKBONE', 'JUNCTION', 'FEEDER', 'DISTRIBUTION', 'DROPCABLE' ];
    obj.forEach(function(item, index){
        result.push({
            name: item
        });
    });
    clientResponse.send(result);
});
router.get('/typeinterface', function (clientRequest, clientResponse, next) {
    const result = [];
    var obj = ['LOGIC', 'PHYSICAL','LAMBDA'];
    obj.forEach(function(item, index){
        result.push({
            name: item
        });
    });
    clientResponse.send(result);
});
router.get('/buildingType', function (clientRequest, clientResponse, next) {
    const result = [];
    var obj = ['LRB'];
    obj.forEach(function(item, index){
        result.push({
            name: item
        });
    });
    clientResponse.send(result);
});

module.exports = router;