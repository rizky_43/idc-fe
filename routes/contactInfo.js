const router = require('express').Router()
const internalService = require('../lib/InternalService')

const dataStructure = [
  {name: 'number', label: 'Contact Identifier', link: 'view', filter: {type: 'text'}},
  {name: 'type', label: 'Contact Type', filter: {type: 'text'}, defaultSort: 'asc'},
  {name: 'issuedBy', label: 'issuedBy', filter: {type: 'text'}}
]

router.get('/data', function(req, res, next) {
  const queries = req.query
  const reqData = {
    limit: 0,
    label: 'ContactInfo',
    skip: queries.start,
    propertyFilter: {},
    propertySort: {}
  }

  if (queries.order) {
    queries.order.forEach(function(item) {
      const field = dataStructure[item.column]
      if (field) {
        reqData.propertySort[field.name] = item.dir
      }
    })
  }

  if (queries.columns) {
    queries.columns.forEach(function(item) {
      if (item.search.value !== '' && item.name) {
        const query = item.search.value.replace(/ /g, '*')
        reqData.propertyFilter[item.name] = '(?i)' + query + '*'
      }
    })
  }

  internalService.listNodes(
    reqData,
    res,
    next,
    function(resData) {
      const data = []

      resData.nodes.forEach(function(item) {
        data.push({
          _id: item.node._id ? item.node._id : null,
          type: item.node.type ? item.node.type : null,
          number: item.node.number ? item.node.number : null,
          issuedBy: item.node.issuedBy ? item.node.issuedBy : null,
          status: item.node.status ? item.node.status: null,
          createdAt: item.node.createdAt ? item.node.createdAt : null,
          createdBy: item.node.createdBy ? item.node.createdBy : null,
          updatedAt: item.node.updatedAt ? item.node.updatedAt : null,
          updatedBy: item.node.updatedBy ? item.node.updatedBy : null,
          links: {
            view: {url: '/contactInfo/' + item.node._id, method: 'GET'}
          }
        })
      })

      const result = {
        draw: queries.draw,
        data: data,
        recordsFiltered: resData.pager.total,
        recordsTotal: resData.pager.total
      }

      if (queries.json === 'true') res.status(200).send(result.data)
      else res.send(result)
    },
    function(err) {
      const result = {
        draw: queries.draw,
        data: [],
        recordsFiltered: 0,
        recordsTotal: 0
      }

      if (err.status !== 404) {
        result.error = err.message
      }

      res.status(200).send(result)
    }
  )
})

router.get('/', function(req, res, next) {
  res.render('contactInfo/list', {
    title: `${serverConfig.server.name.toUpperCase()}`,
    user: res.locals.user,
    dataStructure: dataStructure,
    links: {
      data: {url: '/contactInfo/data', method: 'GET'}
    }
  })
})

router.post('/', function(req, res, next) {
  const reqData = {
    label: 'ContactInfo',
    properties: {
      type: req.body.type,
      number: req.body.number,
      issuedBy: req.body.issuedBy,
      status: 'ACTIVE'
    }
  }

  internalService.createNode(reqData, res, next, function(resData) {
    res.status(200).send({
      links: {
        view: {url: '/contactInfo/' + resData.node._id, method: 'GET'}
      }
    })
  })
})

router.put('/:_id', function(req, res, next) {
  var _id = req.params._id
  var data = {
    label: 'ContactInfo',
    id: parseInt(_id),
    properties: req.body
  }

  internalService.updateNode(data, res, next, function(){
    res.status(200).send({
      links: {
        index: {url: '/contactInfo/' + _id, method: 'GET'}
      }
    })
  })
})

router.get('/:_id', function(req, res, next) {
  var id = req.params._id
  internalService.getNode(id, 'TRUE', res, next, function(resData) {

    // return res.send(resData)
    // Untuk check return data

    const data = []
    
    data.push({
      _id: resData.node._id ? resData.node._id : null,
      type: resData.node.type ? resData.node.type : null,
      number: resData.node.number ? resData.node.number : null,
      issuedBy: resData.node.issuedBy ? resData.node.issuedBy : null,
      status: resData.node.status ? resData.node.status : null,
      createdAt: resData.node.createdAt ? resData.node.createdAt : null,
      createdBy: resData.node.createdBy ? resData.node.createdBy : null,
      updatedAt: resData.node.updatedAt ? resData.node.updatedAt : null,
      updatedBy: resData.node.updatedBy ? resData.node.updatedBy : null,
      links: {
        delete: {url: '/contactInfo/' + resData.node._id, method: 'DELETE'},
        edit: {url: '/contactInfo/' + resData.node._id, method: 'PUT'}
      }
    })


    res.render('contactInfo/details', {
      title: `${serverConfig.server.name.toUpperCase()}`,
      user: res.locals.user,
      data: data[0]
    })    
  }
  )
})

router.delete('/:_id', function(req, res, next) {
  const _id = req.params._id

  internalService.deleteNode(_id, res, next, function() {
    res.status(200).send({
      links: {
        index: {
          url: '/contactInfo/',
          method: 'GET'
        }
      }
    })
  })
})

module.exports = router
