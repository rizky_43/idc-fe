const router = require('express').Router()
const internalService = require('../../lib/InternalService')
const ispService = require('../../lib/ISPService')
const map = require('./function/map')
// this is action button router
// this will be called when the corresponding action button is clicked
router.get('/location/getMoreDetail', function (req, res, next) {
  res.json({ code: 'OK', message: 'Here you go: details, details, details.' })
})

// this is a mandatory router
// this will be called by 'Save <object>' menu from the map
router.post('/location', function (req, res, next) {
  const reqData = {
    label: 'Location',
    properties: req.body.properties
  }

  const reqSpatial = {
    wkt: req.body.wkt,
    objectType: "location",
    key: req.body.properties.uid,
    addInfo: `{"type":"`+req.body.properties.type+`"}`
  }

  internalService.createNode(reqData, res, next, function(resData) {
    ispService.createNode(reqData, res, next, function(resDataIsp) {
      if(resData){
        internalService.spatialAdd(reqSpatial, res, next, function(resData2) {
          if(resData2){
            res.json({ code: 'OK', message: 'Location created.' })
          }else{
            res.json({ code: 'FAILED', message: 'Failed add Spatial' })
          }
        })
      }else{
        res.json({ code: 'FAILED', message: 'Failed create Buildng' })
      }
    })
  })
})

// similar to create, this will be called whenever 'update' action called
router.put('/location', function (req, res, next) {
  //
  // do something here, e.g. call API to update object
  // response will be shown as notification
  //

  // send the response
  res.json({ code: 'OK', message: 'Object updated' })
})

// similar to create, this will be called whenever 'delete' action called
router.delete('/location', function (req, res, next) {
  //
  // do something here, e.g. call API to delete object
  // response will be shown as notification
  //
  map.deleteSpatial('location', req, res, next)
  // send the response
  res.json({ code: 'OK', message: 'Object deleted' })
})

// this is a mandatory router
// this will be called by 'Load Data <object>' menu from the map
router.get('/location', function (req, res, next) {

  map.spatialIntersect('location', req, res, next)

})


module.exports = router
