const router = require('express').Router()
const internalService = require('../../lib/InternalService')
const ispService = require('../../lib/ISPService')
const map = require('./function/map')

// this is action button router
// this will be called when the corresponding action button is clicked
router.get('/building/getMoreDetail', function (req, res, next) {
  res.json({
    code: 'OK',
    message: 'Here you go: details, details, details.'
  })
})

// this is a mandatory router
// this will be called by 'Save <object>' menu from the map
router.post('/building', function (req, res, next) {
  const reqData = {
    label: 'Building',
    properties: req.body.properties
  }

  const reqSpatial = {
    wkt: req.body.wkt,
    objectType: "building",
    key: req.body.properties.uid,
    addInfo: `{"type":"` + req.body.properties.type + `"}`
  }

  internalService.createNode(reqData, res, next, function (resDataBuilding) {
    if (resDataBuilding) {
      ispService.createNode(reqData, res, next, function (resDataBuildingIsp) {
        // create floor automatic
        internalService.spatialAdd(reqSpatial, res, next, function (respSpatial) {
          console.log("Response Add Spatial Building to Spatial DB")
          console.log(respSpatial)
          let reqCreateFloor = {
            collection : 'IDC-FE' ,
            type : 'QUERY',
            name : 'GENERATE_FLOOR',
            body : {
              params : {
                buildingId : parseInt(resDataBuilding.node._id),
                floorcount : parseInt(req.body.properties.floor),
                createdBy : res.locals.user
              }
            }
          }
          internalService.predefinedQuery(reqCreateFloor, res, next, function (respFloor) {
            console.log("Response Create Floor When add new building")
            console.log(respFloor)
            const reqIntersect = {
              wkt : req.body.wkt,
              objectType : "Location"
            }
            internalService.spatialIntersect(reqIntersect, res, next, function (dataIntersect) {
              console.log("Response Data Location Intersect Based On WKT Building")
              console.log(dataIntersect)
              const reqGetLocation = {
                label: "Location",
                propertyFilter: {
                  uid: dataIntersect[0].key
                }
              }
              internalService.listNodes(reqGetLocation, res, next, function (resDataLocation) {
                console.log("Response Data Location based on key from intercect Location UID")
                console.log(resDataLocation)
                console.log(resDataBuilding)
                const paramsDatarel = {
                  startNodeId : resDataBuilding.node._id,
                  endNodeId : resDataLocation.nodes[0].node[0]._id,
                  type : "LOCATED_AT"
                }
                internalService.createRelationship(paramsDatarel, res, next, function (respRelation) {
                  ispService.createRelationship(paramsDatarel, res, next, function (respRelationIsp) {
                    console.log("Response Relation from building to Location")
                    console.log(respRelation)
                    if (respRelation) {
                      res.json({
                        code: 'OK',
                        message: 'Building Created'
                      })
                    }
                  })
                })
              })
            })
          })
        })
      })
    } else {
      res.json({
        code: 'FAILED',
        message: 'Failed create Building'
      })
    }
  })
})

// similar to create, this will be called whenever 'update' action called
router.put('/building', function (req, res, next) {
  //
  // do something here, e.g. call API to update object
  // response will be shown as notification
  //

  // send the response
  res.json({
    code: 'OK',
    message: 'Object updated'
  })
})

// similar to create, this will be called whenever 'delete' action called
router.delete('/building', function (req, res, next) {
  //
  // do something here, e.g. call API to delete object
  // response will be shown as notification
  //

  // send the response
  console.log(req.body)
  res.json({
    code: 'OK',
    message: 'Object deleted'
  })
})

// this is a mandatory router
// this will be called by 'Load Data <object>' menu from the map
router.get('/building', function (req, res, next) {
  map.spatialIntersect('building', req, res, next)
})

module.exports = router